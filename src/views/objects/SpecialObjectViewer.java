package views.objects;

import controllers.objects.ObjectController;
import views.commons.AnimatedObjectViewer;

import java.awt.*;

/**
 *
 * Created by Xander_C on 13/03/2017.
 */
public class SpecialObjectViewer extends ObjectViewer implements AnimatedObjectViewer {

    private int flipAnimationCounter;

    public SpecialObjectViewer(ObjectController controller) {
        super(controller);
    }

    @Override
    public Image animation(){
        return this.getController().getConfig().apply(this);
    }

    @Override
    public int getAnimationStepCounter() {
        return this.flipAnimationCounter;
    }

    @Override
    public void setAnimationStepCounter(int newCounter) {
        this.flipAnimationCounter = newCounter;
        this.flipAnimationCounter %= this.getController().getConfig().getFrequency();
    }
}
