package views.objects;

import controllers.objects.ObjectController;
import views.commons.SimpleObjectViewer;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public class ObjectViewer implements SimpleObjectViewer {

    private final ObjectController controller;

    public ObjectViewer(ObjectController controller){

        this.controller = controller;
    }

    @Override
    public ObjectController getController() {
        return this.controller;
    }
}
