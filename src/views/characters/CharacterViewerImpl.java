package views.characters;

import controllers.characters.CharacterController;
import views.commons.AnimatedObjectViewer;

import java.awt.*;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public class CharacterViewerImpl implements AnimatedObjectViewer<CharacterController> {

    private final CharacterController controller;
    private int stepCounter;

    public CharacterViewerImpl(CharacterController controller){

        this.controller = controller;
        this.stepCounter = 0;
    }

    @Override
    public Image animation() {
        return controller.getConfig().apply(this);
    }

    @Override
    public int getAnimationStepCounter() {
        return this.stepCounter;
    }

    @Override
    public void setAnimationStepCounter(int newCounter){
        this.stepCounter = newCounter;
        this.stepCounter %= this.getController().getConfig().getFrequency();
    }

    @Override
    public CharacterController getController(){
        return this.controller;
    }
}
