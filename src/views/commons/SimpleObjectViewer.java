package views.commons;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public interface SimpleObjectViewer<C>{

    C getController();

}
