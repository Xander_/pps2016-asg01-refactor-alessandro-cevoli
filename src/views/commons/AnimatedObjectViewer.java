package views.commons;

import java.awt.*;

/**
 *
 * Created by Xander_C on 13/03/2017.
 */
public interface AnimatedObjectViewer<C> extends SimpleObjectViewer<C> {

    Image animation();

    int getAnimationStepCounter();

    void setAnimationStepCounter(int newCounter);
}
