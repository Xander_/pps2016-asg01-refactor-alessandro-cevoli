package views.core;

import controllers.core.GameController;
import controllers.core.KeyboardManager;

import javax.swing.*;
import java.awt.*;
import java.util.*;

import static models.configs.EnvironmentConfig.*;

@SuppressWarnings("serial")
public class GamePlatform extends JPanel {

    private GameController controller;

    public GamePlatform(GameController controller){
        this.controller = controller;

        this.setFocusable(true);

        this.requestFocusInWindow();

        java.util.List<Observer> ol = new ArrayList<>();

        ol.add((Observer) this.controller.getModel().getMainCharacter());

        this.addKeyListener(new KeyboardManager(ol));

        this.controller.getModel().populateWorld();
    }

    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        this.controller.computeCollisions();

        this.controller.updateWorldElements();

        this.paintUpdatedWorldElements(g);

    }

    private void paintUpdatedWorldElements(Graphics graphics){

        graphics.drawImage(BACKGROUND_A.getImage(), BACKGROUND_A.getPosition().getX(),
                BACKGROUND_A.getPosition().getY(), null);

        graphics.drawImage(BACKGROUND_B.getImage(), BACKGROUND_B.getPosition().getX(),
                BACKGROUND_B.getPosition().getY(), null);

        graphics.drawImage(STARTING_CASTLE.getImage(),  STARTING_CASTLE.getPosition().getX() - X_POS,
                STARTING_CASTLE.getPosition().getY() , null);

        graphics.drawImage(STARTING_SIGN.getImage(),STARTING_SIGN.getPosition().getX() - X_POS,
                STARTING_SIGN.getPosition().getY(), null);

        graphics.drawImage(ENDING_FLAGPOLE.getImage(), ENDING_FLAGPOLE.getPosition().getX() - X_POS,
                ENDING_FLAGPOLE.getPosition().getY(), null);

        graphics.drawImage(ENDING_CASTLE.getImage(), ENDING_CASTLE.getPosition().getX() - X_POS,
                ENDING_CASTLE.getPosition().getY(), null);

        controller.getModel().getEnvironmentObjectsList().parallelStream()
                .forEach(eo -> graphics.drawImage(eo.getConfig().apply(eo.getViewer()),
                        eo.getController().getX(), eo.getController().getY(), null));

        controller.getModel().getSpecialObjectsList().parallelStream()
                .forEach(s -> graphics.drawImage(s.getViewer().animation(),
                        s.getController().getX(), s.getController().getY(), null));


        controller.getModel().getCharactersList().parallelStream()
                //.filter(c -> c.getController().isAlive())
                .forEach(c -> graphics.drawImage(c.getViewer().animation(),
                        c.getController().getX(),c.getController().getY() + c.getConfig().getDeathOffsetY(c.getController()),
                        null));
    }
}
