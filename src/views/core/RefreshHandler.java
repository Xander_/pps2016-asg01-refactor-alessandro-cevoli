package views.core;

public class RefreshHandler implements Runnable {

    private final int PAUSE = 3;
    private GamePlatform scene;

    public RefreshHandler(GamePlatform scene){

        this.scene = scene;
    }

    public void run() {
        while (true) {
            this.scene.repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

} 
