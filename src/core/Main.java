package core;

import controllers.core.GameControllerImpl;
import models.core.GameModelImpl;
import views.core.GamePlatform;
import views.core.RefreshHandler;

import javax.swing.*;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Main {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario Bros.";

    public static void main(String[] args) {

        boolean debug = false;

        if (!debug){
            JFrame window = new JFrame(WINDOW_TITLE);
            window.setDefaultCloseOperation(EXIT_ON_CLOSE);
            window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
            window.setLocationRelativeTo(null);
            window.setResizable(true);
            window.setAlwaysOnTop(true);

            GamePlatform scene = new GamePlatform(new GameControllerImpl(new GameModelImpl()));
            window.setContentPane(scene);
            window.setVisible(true);

            Thread game = new Thread(new RefreshHandler(scene));
            game.start();


        }
    }

}
