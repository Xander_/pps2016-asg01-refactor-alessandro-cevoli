package core.characters;

import controllers.characters.CharacterController;
import core.commons.GameElement;
import models.configs.CharacterConfig;
import views.commons.AnimatedObjectViewer;

public abstract class BasicCharacter implements GameCharacter {

    private AnimatedObjectViewer<CharacterController> view;

    protected BasicCharacter(final AnimatedObjectViewer<CharacterController> view) {
        this.view = view;

    }

    @Override
    public CharacterConfig getConfig() {
        return this.getViewer().getController().getConfig();
    }

    @Override
    public CharacterController getController() {
        return this.getViewer().getController();
    }

    @Override
    public AnimatedObjectViewer<CharacterController> getViewer() {
        return this.view;
    }

    @Override
    public boolean equals(GameElement<CharacterConfig, CharacterController,
            AnimatedObjectViewer<CharacterController>> gameCharacter) {

        return this.getConfig().getName().equals(gameCharacter.getConfig().getName()) &&
                this.getController().getX() == gameCharacter.getController().getX() &&
                this.getController().getY() == gameCharacter.getController().getY();
    }
}