package core.characters;

import controllers.characters.CharacterController;
import controllers.characters.MainCharController;
import controllers.core.AudioManager;
import controllers.core.KeyboardManager;
import models.configs.AudioConfig;
import models.configs.EnvironmentConfig;
import views.commons.AnimatedObjectViewer;

import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;

public class MainCharacterImpl extends BasicCharacter implements MainCharacter, Observer {

    public MainCharacterImpl(AnimatedObjectViewer<CharacterController> viewer) {
        super(viewer);
    }

    @Override
    public MainCharController getController() {
        return (MainCharController) super.getController();
    }

    @Override
    public void update(Observable o, Object arg) {

        if (o instanceof KeyboardManager && arg instanceof KeyEvent){
            KeyEvent e = (KeyEvent) arg;

            KeyboardManager km = (KeyboardManager) o;

            if(km.isKeyPressed()){
                this.updateOnKeyPressed(e);
            } else if(km.isKeyReleased()){
                this.updateOnKeyReleased(e);
            } else if(km.isKeyTyped()){
                this.updateOnKeyTyped(e);
            }
        }
    }

    private void updateOnKeyPressed(KeyEvent e){

        if (this.getController().isAlive()) {

            if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_D) {

                // per non fare muovere il castello e start
                if (EnvironmentConfig.X_POS == -1) {
                    EnvironmentConfig.X_POS = 0;
                    EnvironmentConfig.BACKGROUND_A.getPosition().setX(-50);
                    EnvironmentConfig.BACKGROUND_B.getPosition().setX(750);
                }

                this.getViewer().setAnimationStepCounter(this.getViewer().getAnimationStepCounter()+1);

                this.getController().setMoving(true);
                this.getController().setToRight(true);

                EnvironmentConfig.MOV = 1; // si muove verso sinistra

            } else if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_A) {

                if (EnvironmentConfig.X_POS == 4601) {
                    EnvironmentConfig.X_POS = 4600;
                    EnvironmentConfig.BACKGROUND_A.getPosition().setX(-50);
                    EnvironmentConfig.BACKGROUND_B.getPosition().setX(750);
                }

                this.getViewer().setAnimationStepCounter(this.getViewer().getAnimationStepCounter()+1);

                this.getController().setMoving(true);
                this.getController().setToRight(false);

                EnvironmentConfig.MOV = -1; // si muove verso destra
            }

            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP ||
                    e.getKeyCode() == KeyEvent.VK_SPACE ||
                    e.getKeyCode() == KeyEvent.VK_W) {
                this.getController().setJumping(true);
                AudioManager.playSound(AudioConfig.AUDIO_JUMP.getSoundSource());
            }

        }

    }

    private void updateOnKeyReleased(KeyEvent e){
        this.getController().setMoving(false);
        EnvironmentConfig.MOV = 0;
    }

    private void updateOnKeyTyped(KeyEvent e){
        // NOPE
    }
}