package core.characters;

import controllers.characters.MainCharController;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public interface MainCharacter extends GameCharacter {

    @Override
    MainCharController getController();

}
