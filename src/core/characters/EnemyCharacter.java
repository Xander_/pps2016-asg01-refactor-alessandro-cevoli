package core.characters;

import controllers.characters.CharacterController;
import models.configs.CharacterConfig;
import views.commons.AnimatedObjectViewer;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public class EnemyCharacter extends BasicCharacter implements Runnable{

    private Thread instance;

    public EnemyCharacter(AnimatedObjectViewer<CharacterController> viewer){
        super(viewer);

        instance = new Thread(this);
        instance.start();
    }

    @Override
    public void run() {
        while (true) {
            if (this.getController().isAlive()) {
                this.getController().move();
                this.getViewer().setAnimationStepCounter(this.getViewer().getAnimationStepCounter()+1);
                try {
                    Thread.sleep(CharacterConfig.PAUSE);
                } catch (InterruptedException e) {
                    // DO NOTHING
                }
            }
        }
    }
}
