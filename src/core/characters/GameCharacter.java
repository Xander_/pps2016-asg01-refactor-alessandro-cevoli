package core.characters;

import controllers.characters.CharacterController;
import core.commons.GameElement;
import models.configs.CharacterConfig;
import views.commons.AnimatedObjectViewer;

public interface GameCharacter
        extends GameElement<CharacterConfig, CharacterController, AnimatedObjectViewer<CharacterController>> {

    // Entry Point to cast GameElement Generics

}
