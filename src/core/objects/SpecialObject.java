package core.objects;

import controllers.objects.ObjectController;
import views.commons.AnimatedObjectViewer;


/**
 *
 * Created by Xander_C on 13/03/2017.
 */
public interface SpecialObject extends GameObject {

    @Override
    AnimatedObjectViewer<ObjectController> getViewer();
}
