package core.objects;

import controllers.objects.ObjectController;
import views.commons.AnimatedObjectViewer;

public class SpecialObjectImpl extends GameObjectImpl implements SpecialObject, Runnable {

    private static final int PAUSE = 10;

    public SpecialObjectImpl(AnimatedObjectViewer viewer) {
        super(viewer);
    }

    @Override
    public AnimatedObjectViewer<ObjectController> getViewer(){
        return (AnimatedObjectViewer<ObjectController>) super.getViewer();
    }

    @Override
    public void run() {
        while (true) {
            this.getViewer().setAnimationStepCounter(this.getViewer().getAnimationStepCounter()+1);
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

}
