package core.objects;

import controllers.objects.ObjectController;
import core.commons.GameElement;
import models.configs.ObjectConfig;
import views.commons.SimpleObjectViewer;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public interface GameObject
        extends GameElement<ObjectConfig, ObjectController, SimpleObjectViewer<ObjectController>>{

    // Entry Point to cast Game Element Generics


}
