package core.objects;

import controllers.objects.ObjectController;
import core.commons.GameElement;
import models.configs.ObjectConfig;
import views.commons.SimpleObjectViewer;

public class GameObjectImpl implements GameObject {

    private SimpleObjectViewer<ObjectController> viewer;

    public GameObjectImpl(
            final SimpleObjectViewer<ObjectController> viewer) {

        this.viewer = viewer;
    }

    @Override
    public ObjectController getController() {
        return getViewer().getController();
    }

    @Override
    public ObjectConfig getConfig() {
        return getViewer().getController().getConfig();
    }

    @Override
    public SimpleObjectViewer<ObjectController> getViewer() {
        return viewer;
    }

    @Override
    public boolean equals(GameElement<ObjectConfig, ObjectController, SimpleObjectViewer<ObjectController>> gameElement) {
        return this.getConfig().getName().equals(gameElement.getConfig().getName()) &&
                this.getController().getX() == gameElement.getController().getX() &&
                this.getController().getY() == gameElement.getController().getY();
    }
}
