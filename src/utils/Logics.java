package utils;

import controllers.characters.CharacterController;
import models.configs.CharacterConfig;
import models.configs.EnvironmentConfig;
import models.configs.Res;
import views.commons.AnimatedObjectViewer;

import java.awt.*;

import static models.configs.Res.*;
import static models.configs.Res.buildDeathImage;

/**
 *
 * Created by Alessandro on 18/03/2017.
 */
public class Logics {

    public static Image jumpAnimation(AnimatedObjectViewer<CharacterController> v){

        Image image = v.getController().isToRight() ?
                buildJumpImage(v.getController().getConfig().getName(), Res.DIR_DX) :
                buildJumpImage(v.getController().getConfig().getName(), Res.DIR_SX);

        v.getController().setJumpExtent(v.getController().getJumpingExtent()+1);

        if (v.getController().getJumpingExtent() < CharacterConfig.JUMPING_LIMIT) {

            if (v.getController().getY() > EnvironmentConfig.HEIGHT_LIMIT) {
                v.getController().setY(v.getController().getY() - 4);
            } else {
                v.getController().setJumpExtent(CharacterConfig.JUMPING_LIMIT);
            }

        } else if (v.getController().getY() + v.getController().getHeight() < EnvironmentConfig.FLOOR_OFFSET_Y) {

            v.getController().setY(v.getController().getY() + 1);

        } else {

            v.getController().setJumping(false);
            v.getController().setJumpExtent(0);

            image = v.getController().isToRight() ?
                    buildIdleImage(v.getController().getConfig().getName(), Res.DIR_DX) :
                    buildIdleImage(v.getController().getConfig().getName(), Res.DIR_SX);
        }

        return image;
    }

    public static Image walkAnimation(AnimatedObjectViewer<CharacterController> v){

        final String direction = v.getController().isToRight() ? Res.DIR_DX : Res.DIR_SX;

        return !v.getController().isMoving() ||
                v.getAnimationStepCounter() % v.getController().getConfig().getFrequency() == 0 ?
                buildIdleImage(v.getController().getConfig().getName(), direction) :
                buildWalkImage(v.getController().getConfig().getName(), direction);
    }

    public static Image deathAnimation(AnimatedObjectViewer<CharacterController> v){

        return v.getController().isToRight() ? buildDeathImage(v.getController().getConfig().getName(), Res.DIR_DX) :
                buildDeathImage(v.getController().getConfig().getName(), Res.DIR_SX);
    }
}
