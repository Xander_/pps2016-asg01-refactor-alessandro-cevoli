package utils;

/**
 *
 * Created by Xander_C on 28/02/2017.
 */
public interface Pair<X, Y> {

    X getFirst();

    Y getSecond();

    void setFirst(X first);

    void setSecond(Y second);
}
