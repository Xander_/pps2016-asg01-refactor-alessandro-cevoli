package controllers.core;

import models.core.GameModel;

/**
 *
 * Created by Xander_C on 13/03/2017.
 */
public interface GameController {

    void computeCollisions();

    void updateWorldElements();

    GameModel getModel();

}
