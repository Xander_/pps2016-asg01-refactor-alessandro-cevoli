package controllers.core;


import core.objects.SpecialObject;
import models.configs.AudioConfig;
import models.core.GameModel;

import java.util.Iterator;

import static models.configs.EnvironmentConfig.*;

/**
 *
 * Created by Xander_C on 13/03/2017.
 */
public class GameControllerImpl implements GameController {

    private GameModel model;

    public GameControllerImpl(GameModel model){

        this.model = model;
    }

    @Override
    public void computeCollisions() {


        model.getCharactersList()
                .forEach(c -> model.getEnvironmentObjectsList().stream()
                        .filter(eo -> c.getController().isNearby(eo.getController()))
                        .forEach(eo -> c.getController().computeCollision(eo.getController())));

        model.getCharactersList()
                .forEach(cA -> model.getCharactersList().stream()
                        .filter(cB -> !cA.equals(cB) && cA.getController().isNearby(cB.getController()))
                        .forEach(cB -> cA.getController().computeCollision(cB.getController())));

        for (Iterator<SpecialObject> iterator = model.getSpecialObjectsList().listIterator(); iterator.hasNext();) {

            if (model.getMainCharacter().getController().
                    collisionWithASpecialObject(iterator.next().getController())){

                AudioManager.playSound(AudioConfig.AUDIO_COIN.getSoundSource());
                iterator.remove();
                //this.monies.remove(m); // NOT THREAD SAFE
            }
        }
    }

    @Override
    public void updateWorldElements() {

        this.updateBackgroundOnMovement();

        this.moveCPUControlledElements();

    }

    private void updateBackgroundOnMovement() {
        if (X_POS >= 0 && X_POS <= 4600) {
            X_POS += MOV;

            // Moving the screen to give the impression that MainCharacterImpl is walking
            BACKGROUND_A.getPosition().setX(BACKGROUND_A.getPosition().getX() - MOV);
            BACKGROUND_B.getPosition().setX(BACKGROUND_B.getPosition().getX() - MOV);
        }

        // Flipping between background1 and background2
        if (BACKGROUND_A.getPosition().getX() == -BACKGROUND_FLIPPING_OFFSET) {
            BACKGROUND_A.getPosition().setX(BACKGROUND_FLIPPING_OFFSET);

        } else if (BACKGROUND_B.getPosition().getX() == -BACKGROUND_FLIPPING_OFFSET) {
            BACKGROUND_B.getPosition().setX(BACKGROUND_FLIPPING_OFFSET);

        } else if (BACKGROUND_A.getPosition().getX() == BACKGROUND_FLIPPING_OFFSET) {
            BACKGROUND_A.getPosition().setX(-BACKGROUND_FLIPPING_OFFSET);

        } else if (BACKGROUND_B.getPosition().getX() == BACKGROUND_FLIPPING_OFFSET) {
            BACKGROUND_B.getPosition().setX(-BACKGROUND_FLIPPING_OFFSET);
        }
    }

    private void moveCPUControlledElements(){
        if (X_POS >= 0 && X_POS <= 4600) {

            model.getEnvironmentObjectsList().parallelStream()
                    .forEach(eo-> eo.getController().move());
            model.getSpecialObjectsList().parallelStream()
                    .forEach(s -> s.getController().move());
            model.getCharactersList().parallelStream()
                    //.filter(c->c.getController().isAlive())
                    .forEach(c -> c.getController().move());
        }
    }

    @Override
    public GameModel getModel() {
        return this.model;
    }
}
