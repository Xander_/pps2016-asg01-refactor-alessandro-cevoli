package controllers.objects;

import controllers.commons.BasicControllerImpl;
import models.commons.BoundingBox;
import models.commons.Dimensions;
import models.commons.Point;
import models.configs.EnvironmentConfig;
import models.configs.ObjectConfig;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public class ObjectControllerImpl extends BasicControllerImpl<ObjectConfig>
        implements ObjectController {

    private ObjectConfig config;

    public ObjectControllerImpl(ObjectConfig config, Point<Integer> position) {
        super(new BoundingBox<>(new Dimensions<>(config.getWidth(), config.getHeight()),
                position));
        this.config = config;
    }

    @Override
    public Point<Integer> move() {
        if (EnvironmentConfig.X_POS >= 0) {
            this.setX(this.getX() - EnvironmentConfig.MOV);
        }

        return new Point<>(this.getX(), this.getY());
    }

    @Override
    public ObjectConfig getConfig(){
        return this.config;
    }

    @Override
    public boolean equals(ObjectController environmentalObject) {
        return getConfig().getName().equals(environmentalObject.getConfig().getName()) &&
                getX() == environmentalObject.getX() && getY() == environmentalObject.getY();
    }
}
