package controllers.objects;

import controllers.commons.BasicController;
import models.configs.ObjectConfig;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public interface ObjectController extends BasicController<ObjectConfig> {

    boolean equals(ObjectController environmentalObject);
}
