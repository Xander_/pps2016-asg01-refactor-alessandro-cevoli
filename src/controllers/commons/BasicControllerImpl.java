package controllers.commons;

import models.commons.BoundingBox;
import models.commons.Dimensions;
import models.commons.Point;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public abstract class BasicControllerImpl<C> implements BasicController<C> {

    private BoundingBox<Integer> boundingBox;

    public BasicControllerImpl(BoundingBox<Integer> boundingBox) {
        this.boundingBox = boundingBox;
    }

    public int getWidth() {
        return boundingBox.getDimensions().getFirst();
    }

    public int getHeight() {
        return boundingBox.getDimensions().getSecond();
    }

    public int getX() {
        return boundingBox.getPosition().getX();
    }

    public int getY() {
        return boundingBox.getPosition().getY();
    }

    public void setWidth(int width) {
        this.boundingBox.setDimensions(new Dimensions<>(width, this.getHeight()));
    }

    public void setHeight(int height) {
        this.boundingBox.setDimensions(new Dimensions<>(this.getWidth(), height));
    }

    public void setX(int x) {
        this.boundingBox.setPosition(new Point<>(x, this.getY()));
    }

    public void setY(int y) {
        this.boundingBox.setPosition(new Point<>(this.getX(), y));
    }

}
