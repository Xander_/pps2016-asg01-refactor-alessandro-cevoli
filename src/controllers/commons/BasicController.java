package controllers.commons;

import models.commons.Point;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public interface BasicController<C> {

    int getWidth();

    int getHeight();

    int getX();

    int getY();

    void setWidth(int width);

    void setHeight(int height);

    void setX(int x);

    void setY(int y);

    /**
     *
     * @return The new position of the object
     */
    Point<Integer> move();

    C getConfig();

}
