package controllers.characters;

import controllers.commons.BasicController;
import models.configs.CharacterConfig;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public interface CharacterController extends BasicController<CharacterConfig> {

    boolean isAlive();

    void setAlive(boolean isAlive);

    boolean isMoving();

    void setMoving(boolean isMoving);

    boolean isToRight();

    void setToRight(boolean isMovingRight);

    boolean isJumping();

    void setJumping(boolean jumping);

    int getJumpingExtent();

    void setJumpExtent(int newExtent);

    boolean isNearby(BasicController<?> basicController);

    boolean computeCollision(BasicController<?> basicController);

}
