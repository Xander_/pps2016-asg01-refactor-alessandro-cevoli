package controllers.characters;

import controllers.commons.BasicController;
import models.commons.Point;
import models.configs.CharacterConfig;
import models.configs.EnvironmentConfig;
import models.configs.ObjectConfig;
import utils.Utils;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public class MainCharControllerImpl extends CharacterControllerImpl implements MainCharController {

    private static final int MAIN_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;

    public MainCharControllerImpl(CharacterConfig config, Point<Integer> position){

        super(config, position);

    }

    @Override
    public Point<Integer> move(){
        // DO NOTHING
        return new Point<>(this.getX(), this.getY());
    }

    @Override
    public boolean collisionWithASpecialObject(BasicController<ObjectConfig> specialObject) {

        return Utils.hitBack(this, specialObject) || Utils.hitAbove(this, specialObject)
                || Utils.hitAhead(this, specialObject) || Utils.hitBelow(this, specialObject);

    }

    @Override
    public boolean computeCollision(BasicController<?> basicController) {
        if(basicController instanceof CharacterController){
            return this.contactWithACharacter((CharacterController) basicController);
        } else {
            return this.contactWithAnEnvironmentalObject(basicController);
        }
    }

    private boolean contactWithAnEnvironmentalObject(BasicController<?> environmentalObject) {

        boolean ret = false;

        if (Utils.hitBelow(this, environmentalObject) && this.isJumping()) {

            EnvironmentConfig.FLOOR_OFFSET_Y = environmentalObject.getY();
            ret = true;

        } else if (!Utils.hitBelow(this, environmentalObject)) {

            EnvironmentConfig.FLOOR_OFFSET_Y = FLOOR_OFFSET_Y_INITIAL;

            if (!this.isJumping()) {
                this.setY(MAIN_OFFSET_Y_INITIAL);
            }

            if (Utils.hitAbove(this, environmentalObject)) {
                EnvironmentConfig.HEIGHT_LIMIT = environmentalObject.getY() + environmentalObject.getHeight(); // the new sky goes below the object
            } else if (!Utils.hitAbove(this, environmentalObject) && !this.isJumping()) {
                EnvironmentConfig.HEIGHT_LIMIT = 0; // initial sky
            }

            ret = true;
        }

        if (Utils.hitAhead(this, environmentalObject) && this.isToRight() ||
                Utils.hitBack(this, environmentalObject) && !this.isToRight()) {

            EnvironmentConfig.MOV = 0;
            this.setMoving(false);
            ret = true;
        }

        return ret;
    }

    private boolean contactWithACharacter(CharacterController pers) {

        if (Utils.hitBelow(this, pers)) {

            pers.setMoving(false);
            pers.setAlive(false);

            return true;

        } else if ((Utils.hitAhead(this, pers) || Utils.hitBack(this, pers))
                && pers.isAlive() && this.isAlive()) {

            this.setMoving(false);
            this.setAlive(false);

            return true;
        }

        return false;
    }
}
