package controllers.characters;

import controllers.commons.BasicController;
import models.commons.Point;
import models.configs.CharacterConfig;
import utils.Utils;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public class EnemyControllerImpl extends CharacterControllerImpl {

    private int offsetX;

    public EnemyControllerImpl(CharacterConfig config, Point<Integer> position) {
        super(config, position);

        super.setToRight(true);
        super.setMoving(true);
        this.offsetX = 1;
    }

    @Override
    public Point<Integer> move() {
        this.offsetX = this.isToRight() ? 1 : -1;
        super.setX(super.getX() + this.offsetX);

        return new Point<>(this.getX(), this.getY());
    }

    @Override
    public boolean computeCollision(BasicController<?> obj) {
        if (Utils.hitAhead(this, obj) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;

            return true;

        } else if (Utils.hitBack(this, obj) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;

            return true;
        }

        return false;
    }
}
