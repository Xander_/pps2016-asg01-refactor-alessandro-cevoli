package controllers.characters;

import controllers.commons.BasicController;
import controllers.commons.BasicControllerImpl;
import models.commons.BoundingBox;
import models.commons.Dimensions;
import models.commons.Point;
import models.configs.CharacterConfig;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public abstract class CharacterControllerImpl extends BasicControllerImpl<CharacterConfig>
        implements CharacterController {

    private static final int PROXIMITY_MARGIN = 10;
    //private static final int HIT_BOX_OFFSET = 4;
    private final CharacterConfig config;

    private boolean moving;
    private boolean toRight;
    private boolean alive;
    private boolean jumping;
    private int jumpingExtent;

    public CharacterControllerImpl(CharacterConfig config, Point<Integer> position){

        super(new BoundingBox<>(new Dimensions<>(config.getWidth(), config.getHeight()),
                position));

        this.config = config;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    @Override
    public CharacterConfig getConfig(){
        return this.config;
    }

    @Override
    public boolean isNearby(BasicController basicController) {
        return (this.getX() > basicController.getX() - PROXIMITY_MARGIN &&
                this.getX() < basicController.getX() + basicController.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.getWidth() > basicController.getX() - PROXIMITY_MARGIN &&
                        this.getX() + this.getWidth() < basicController.getX() + basicController.getWidth() + PROXIMITY_MARGIN);
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public void setAlive(boolean isAlive) {
        this.alive = isAlive;
    }

    @Override
    public void setMoving(boolean isMoving) {
        this.moving = isMoving;
    }

    @Override
    public boolean isMoving() {
        return moving;
    }

    @Override
    public boolean isToRight() {
        return toRight;
    }

    @Override
    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    @Override
    public boolean isJumping() {
        return jumping;
    }

    @Override
    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }


    @Override
    public int getJumpingExtent() {
        return this.jumpingExtent;
    }

    @Override
    public void setJumpExtent(int newExtent) {
        this.jumpingExtent = newExtent;
    }

}
