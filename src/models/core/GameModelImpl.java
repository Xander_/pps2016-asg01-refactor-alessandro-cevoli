package models.core;

import core.characters.GameCharacter;
import core.characters.MainCharacter;
import core.objects.GameObject;
import core.objects.SpecialObject;
import models.commons.Point;
import models.factories.GameElementsFactory;
import models.factories.GameElementsFactoryImpl;

import java.util.ArrayList;
import java.util.List;

import static models.configs.CharacterConfig.*;
import static models.configs.ObjectConfig.*;

/**
 *
 * Created by Xander_C on 13/03/2017.
 */
public class GameModelImpl implements GameModel {

    private final List<GameCharacter> characters;
    private final List<GameObject> objects;
    private final List<SpecialObject> specialObjects;
    private final MainCharacter mainCharacter;
    private final GameElementsFactory factory;

    public GameModelImpl(){

        this.characters = new ArrayList<>();
        this.objects = new ArrayList<>();
        this.specialObjects = new ArrayList<>();

        factory = new GameElementsFactoryImpl();

        this.mainCharacter = factory.createNewPlayerCharacter(MARIO, new Point<>(300, 245));
    }

    @Override
    public void populateWorld() {

        this.objects.add(factory.createNewEnvironmentalObject(PIPE, new Point<>(600, 230)));
        this.objects.add(factory.createNewEnvironmentalObject(PIPE, new Point<>(1000, 230)));
        this.objects.add(factory.createNewEnvironmentalObject(PIPE, new Point<>(1600, 230)));
        this.objects.add(factory.createNewEnvironmentalObject(PIPE, new Point<>(1900, 230)));
        this.objects.add(factory.createNewEnvironmentalObject(PIPE, new Point<>(2500, 230)));
        this.objects.add(factory.createNewEnvironmentalObject(PIPE, new Point<>(3000, 230)));
        this.objects.add(factory.createNewEnvironmentalObject(PIPE, new Point<>(3800, 230)));
        this.objects.add(factory.createNewEnvironmentalObject(PIPE, new Point<>(4500, 230)));

        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(400, 180)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(1200, 180)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(1270, 170)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(1340, 160)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(2000, 180)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(2600, 160)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(2650, 180)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(3500, 160)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(3550, 140)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(4000, 170)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(4200, 200)));
        this.objects.add(factory.createNewEnvironmentalObject(BLOCK, new Point<>(4300, 210)));

        this.specialObjects.add(factory.createNewSpecialObject(COIN, new Point<>(402, 145)));
        this.specialObjects.add(factory.createNewSpecialObject(COIN, new Point<>(1202, 140)));
        this.specialObjects.add(factory.createNewSpecialObject(COIN, new Point<>(1272, 95)));
        this.specialObjects.add(factory.createNewSpecialObject(COIN, new Point<>(1342, 40)));
        this.specialObjects.add(factory.createNewSpecialObject(COIN, new Point<>(1650, 145)));
        this.specialObjects.add(factory.createNewSpecialObject(COIN, new Point<>(2650, 145)));
        this.specialObjects.add(factory.createNewSpecialObject(COIN, new Point<>(3000, 135)));
        this.specialObjects.add(factory.createNewSpecialObject(COIN, new Point<>(3400, 125)));
        this.specialObjects.add(factory.createNewSpecialObject(COIN, new Point<>(4200, 145)));
        this.specialObjects.add(factory.createNewSpecialObject(COIN, new Point<>(4600, 40)));

        this.characters.add(mainCharacter);
        this.characters.add(factory.createNewEnemyCharacter(GOOMBA, new Point<>(800, 263)));
        this.characters.add(factory.createNewEnemyCharacter(KOOPA, new Point<>(950, 243)));
        this.characters.add(factory.createNewEnemyCharacter(KOOPA, new Point<>(1050, 243)));
    }

    @Override
    public List<GameObject> getEnvironmentObjectsList() {
        return this.objects;
    }

    @Override
    public List<GameCharacter> getCharactersList() {
        return this.characters;
    }

    @Override
    public List<SpecialObject> getSpecialObjectsList() {
        return this.specialObjects;
    }

    @Override
    public MainCharacter getMainCharacter() {
        return this.mainCharacter;
    }
}
