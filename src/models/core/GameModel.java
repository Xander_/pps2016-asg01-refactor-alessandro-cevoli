package models.core;

import core.characters.GameCharacter;
import core.characters.MainCharacter;
import core.objects.GameObject;
import core.objects.SpecialObject;

import java.util.List;

/**
 *
 * Created by Xander_C on 13/03/2017.
 */
public interface GameModel {

    void populateWorld();

    List<GameObject> getEnvironmentObjectsList();

    List<GameCharacter> getCharactersList();

    List<SpecialObject> getSpecialObjectsList();

    MainCharacter getMainCharacter();



}
