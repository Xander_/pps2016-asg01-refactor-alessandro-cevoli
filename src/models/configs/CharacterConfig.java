package models.configs;

import controllers.characters.CharacterController;
import models.commons.Dimensions;
import utils.Logics;
import views.commons.AnimatedObjectViewer;

import java.awt.*;
import java.util.function.Function;

import static models.configs.Res.*;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */

public enum CharacterConfig
        implements Function<AnimatedObjectViewer<CharacterController>, Image>{

    MARIO(Res.MARIO,
            new Dimensions<>(28, 50), 5, 0,
            view -> view.getController().isAlive() ?
                    view.getController().isJumping() ?
                            Logics.jumpAnimation(view) : Logics.walkAnimation(view) : Logics.deathAnimation(view)),
    
    KOOPA(Res.KOOPA,
            new Dimensions<>(43, 50), 15, 30,
            view -> view.getController().isAlive() ?
                    Logics.walkAnimation(view) : Logics.deathAnimation(view)),

    GOOMBA(Res.GOOMBA,
            new Dimensions<>(27, 30), 15, 20,
            view -> view.getController().isAlive() ?
                    Logics.walkAnimation(view) : Logics.deathAnimation(view));

    public static final int PAUSE = 15;
    public static final int JUMPING_LIMIT = 50;

    private String name;
    private Dimensions<Integer> dimensions;
    private int frequency;
    private int deathOffsetY;
    private Function<AnimatedObjectViewer<CharacterController>, Image> animation;

            CharacterConfig(String name, Dimensions<Integer> dimensions,
                    int frequency, int deathOffsetY,
                    Function<AnimatedObjectViewer<CharacterController>, Image> animation){

        this.name = name;
        this.dimensions = dimensions;
        this.frequency = frequency;
        this.deathOffsetY = deathOffsetY;
        this.animation = animation;
    }

    @Override
    public Image apply(AnimatedObjectViewer<CharacterController> view){
        return this.animation.apply(view);
    }

    public String getName() {
        return name;
    }

    public int getFrequency() {
        return frequency;
    }

    public int getDeathOffsetY(CharacterController character) {
        return character.isAlive()? 0 : deathOffsetY;
    }

    public int getWidth() {
        return dimensions.getFirst();
    }

    public int getHeight() {
        return dimensions.getSecond();
    }
}