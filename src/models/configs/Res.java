package models.configs;

import utils.Utils;

import java.awt.*;

/**
 * @author Roberto Casadei
 */

public class Res {

    //public static final String sep = "/";
    private static final String SPRITES_ROOT = "/sprites/";
    private static final String AUDIO_ROOT = "/audio/";
    private static final String SPRITE_EXTENTION = ".png";

    //public static final String IMGP_STATUS_NORMAL = "";
    private static final String STATUS_ACTIVE = "Walking";
    private static final String STATUS_DEAD = "Dead";
    private static final String STATUS_IDLE = "Idle";
    private static final String STATUS_JUMP = "Jumping";

    public static final String DIR_SX = "Left";
    public static final String DIR_DX = "Right";

    public static final String GOOMBA = "Goomba";
    public static final String KOOPA = "Koopa";
    public static final String MARIO = "Mario";

    public static final String BLOCK = "Block";
    public static final String COIN = "Coin";
    public static final String MONEY1 = COIN +"1";
    public static final String MONEY2 = COIN +"2";
    public static final String PIPE = "Pipe";

    public static final String IMG_BLOCK = SPRITES_ROOT + BLOCK + SPRITE_EXTENTION;

    public static final String IMG_COIN1 = SPRITES_ROOT + MONEY1 + SPRITE_EXTENTION;
    public static final String IMG_COIN2 = SPRITES_ROOT + MONEY2 + SPRITE_EXTENTION;
    public static final String IMG_PIPE = SPRITES_ROOT + PIPE + SPRITE_EXTENTION;

    public static final String IMG_BACKGROUND = SPRITES_ROOT + "Background" + SPRITE_EXTENTION;
    public static final String IMG_CASTLE_START = SPRITES_ROOT + "StartingCastle" + SPRITE_EXTENTION;
    public static final String START_ROADSIGN = SPRITES_ROOT + "StartRoadSign" + SPRITE_EXTENTION;
    public static final String IMG_CASTLE_FINAL = SPRITES_ROOT + "EndingCastle" + SPRITE_EXTENTION;
    public static final String IMG_FLAGPOLE = SPRITES_ROOT + "FlagPole" + SPRITE_EXTENTION;

    public static final String AUDIO_COIN = AUDIO_ROOT + "coin.wav";
    public static final String AUDIO_MARIO = AUDIO_ROOT + "mario.wav";
    public static final String AUDIO_JUMP = AUDIO_ROOT + "jump.wav";

    public static Image buildIdleImage(String charName, String direction){
        return Utils.getImage(SPRITES_ROOT + charName + STATUS_IDLE + direction + SPRITE_EXTENTION);
    }

    public static Image buildWalkImage(String charName, String direction){
        return Utils.getImage(SPRITES_ROOT + charName + STATUS_ACTIVE + direction + SPRITE_EXTENTION);
    }

    public static Image buildJumpImage(String charName, String direction){
        return Utils.getImage(SPRITES_ROOT + charName + STATUS_JUMP + direction + SPRITE_EXTENTION);
    }

    public static Image buildDeathImage(String charName, String direction){
        return Utils.getImage(SPRITES_ROOT + charName + STATUS_DEAD + direction + SPRITE_EXTENTION);
    }
}
