package models.configs;

import controllers.objects.ObjectController;
import models.commons.Dimensions;
import views.commons.AnimatedObjectViewer;
import views.commons.SimpleObjectViewer;

import java.awt.*;
import java.util.function.Function;

import static utils.Utils.getImage;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public enum ObjectConfig implements Function<SimpleObjectViewer<ObjectController>, Image> {

    PIPE(Res.PIPE,
            new Dimensions<>(43, 65), 0,
            view -> getImage(Res.IMG_PIPE)),

    BLOCK(Res.BLOCK,
            new Dimensions<>(30, 30), 0,
            view -> getImage(Res.IMG_BLOCK)),
    COIN(Res.COIN,
            new Dimensions<>(30, 30), 20,
            view ->{
                if (view instanceof AnimatedObjectViewer){
                    AnimatedObjectViewer<ObjectController> v = (AnimatedObjectViewer<ObjectController>) view;
                    return v.getAnimationStepCounter() % v.getController().getConfig().getFrequency()  == 0 ?
                            getImage(Res.IMG_COIN1) : getImage(Res.IMG_COIN2);
                } else {
                    return getImage(Res.IMG_COIN1);
                }
            });

    private String name;
    private Dimensions<Integer> dimensions;
    private int frequency;
    private Function<SimpleObjectViewer<ObjectController>, Image> animation;

    ObjectConfig(String name,
                 Dimensions<Integer> dimensions, int frequency,
                 Function<SimpleObjectViewer<ObjectController>, Image> animation){

        this.name = name;
        this.dimensions = dimensions;
        this.frequency = frequency;
        this.animation = animation;
    }

    @Override
    public Image apply(SimpleObjectViewer<ObjectController> view){
        return this.animation.apply(view);
    }

    public Integer getWidth() {
        return dimensions.getFirst();
    }

    public Integer getHeight(){
        return dimensions.getSecond();
    }

    public int getFrequency(){
        return this.frequency;
    }

    public String getName() {
        return name;
    }
}
