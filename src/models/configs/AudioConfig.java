package models.configs;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public enum AudioConfig {
    AUDIO_MARIO(Res.AUDIO_MARIO),
    AUDIO_COIN(Res.AUDIO_COIN),
    AUDIO_JUMP(Res.AUDIO_JUMP);

    private String soundSource;

    AudioConfig(String soundSource){

        this.soundSource = soundSource;
    }

    public String getSoundSource() {
        return soundSource;
    }
}
