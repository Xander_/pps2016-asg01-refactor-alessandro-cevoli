package models.configs;

import models.commons.Point;
import utils.Utils;

import java.awt.*;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public enum EnvironmentConfig {

    BACKGROUND_A(Res.IMG_BACKGROUND, new Point<>(-50,0)),
    BACKGROUND_B(Res.IMG_BACKGROUND, new Point<>(750,0)),
    STARTING_CASTLE(Res.IMG_CASTLE_START, new Point<>(10,95)),
    ENDING_CASTLE(Res.IMG_CASTLE_FINAL, new Point<>(4850, 145)),
    STARTING_SIGN(Res.START_ROADSIGN, new Point<>(220,234)),
    ENDING_FLAGPOLE(Res.IMG_FLAGPOLE, new Point<>(4650, 115));

    public static int MOV = 0;
    public static int X_POS = -1;
    public static int FLOOR_OFFSET_Y = 293;
    public static int HEIGHT_LIMIT = 0;
    public static final int BACKGROUND_FLIPPING_OFFSET = 800;

    private String imageSource;
    private Point<Integer> position;

    EnvironmentConfig(String imageSource, Point<Integer> position){

        this.imageSource = imageSource;
        this.position = position;
    }

    public Image getImage(){
        return Utils.getImage(this.imageSource);
    }


    public Point<Integer> getPosition() {
        return this.position;
    }
}
