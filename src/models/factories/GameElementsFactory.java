package models.factories;


import core.characters.GameCharacter;
import core.characters.MainCharacter;
import core.objects.GameObject;
import core.objects.SpecialObject;
import models.commons.Point;
import models.configs.CharacterConfig;
import models.configs.ObjectConfig;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public interface GameElementsFactory {

    MainCharacter createNewPlayerCharacter(CharacterConfig config, Point<Integer> position);

    GameCharacter createNewEnemyCharacter(CharacterConfig config, Point<Integer> position);

    GameObject createNewEnvironmentalObject(ObjectConfig config, Point<Integer> position);

    SpecialObject createNewSpecialObject(ObjectConfig specialObject, Point<Integer> position);

}
