package models.factories;

import controllers.characters.EnemyControllerImpl;
import controllers.characters.MainCharControllerImpl;
import controllers.objects.ObjectControllerImpl;
import core.characters.EnemyCharacter;
import core.characters.GameCharacter;
import core.characters.MainCharacter;
import core.characters.MainCharacterImpl;
import core.objects.GameObject;
import core.objects.GameObjectImpl;
import core.objects.SpecialObject;
import core.objects.SpecialObjectImpl;
import models.commons.Point;
import models.configs.CharacterConfig;
import models.configs.ObjectConfig;
import views.characters.CharacterViewerImpl;
import views.objects.ObjectViewer;
import views.objects.SpecialObjectViewer;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public class GameElementsFactoryImpl implements GameElementsFactory {

    @Override
    public MainCharacter createNewPlayerCharacter(CharacterConfig config, Point<Integer> position){

        return new MainCharacterImpl(new CharacterViewerImpl(new MainCharControllerImpl(config, position)));
    }

    @Override
    public GameCharacter createNewEnemyCharacter(CharacterConfig config, Point<Integer> position) {
        return new EnemyCharacter(new CharacterViewerImpl(new EnemyControllerImpl(config, position)));
    }

    @Override
    public GameObject createNewEnvironmentalObject(ObjectConfig config, Point<Integer> position) {
        return new GameObjectImpl(new ObjectViewer(new ObjectControllerImpl(config, position)));
    }

    @Override
    public SpecialObject createNewSpecialObject(ObjectConfig config, Point<Integer> position) {
        return new SpecialObjectImpl(new SpecialObjectViewer(new ObjectControllerImpl(config, position)));
    }
}
