package models.commons;

import utils.Pair;

/**
 *
 * Created by Xander_C on 28/02/2017.
 */
public class Dimensions<X> implements Pair<X, X> {

    private X length;
    private X height;

    public Dimensions(final X length, final X height){
        this.setFirst(length);
        this.setSecond(height);
    }

    /**
     *
     * @return the length of this pair of dimensions
     */
    @Override
    public X getFirst(){
        return this.length;
    }

    /**
     *
     * @return the height of this pair of dimensions
     */
    @Override
    public X getSecond(){
        return this.height;
    }

    /**
     * Set the length of this pair of dimensions
     *
     * @param length
     */
    @Override
    public void setFirst(X length) {
        this.length = length;
    }

    /**
     * Set the height of this pair of dimensions
     * @param height
     */
    @Override
    public void setSecond(X height) {
        this.height = height;
    }
}
