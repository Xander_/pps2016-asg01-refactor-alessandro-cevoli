package models.commons;

/**
 *
 * Created by Xander_C on 28/02/2017.
 */
public class Point<T> {

    private T x;
    private T y;


    public Point(final T x, final T y){
        this.x = x;
        this.y = y;
    }

    public T getX() {
        return x;
    }

    public T getY() {
        return y;
    }

    public void setX(T x) {
        this.x = x;
    }

    public void setY(T y) {
        this.y = y;
    }
}
