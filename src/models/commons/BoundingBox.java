package models.commons;

/**
 *
 * Created by Xander_C on 09/03/2017.
 */
public class BoundingBox<X> {

    private Point<X> position;
    private Dimensions<X> dimensions;

    public BoundingBox(final Dimensions<X> dimensions, final Point<X> position){

        this.dimensions = dimensions;
        this.position = position;

    }

    public Point<X> getPosition(){
        return this.position;
    }

    public void setPosition(final Point<X> newPosition){
        this.position = newPosition;
    }

    public Dimensions<X> getDimensions(){
        return this.dimensions;
    }

    public void setDimensions(final Dimensions<X> newDimensions){
        this.dimensions = newDimensions;
    }
}
